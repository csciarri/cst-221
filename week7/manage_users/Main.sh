#!/bin/sh
while IFS= read -r line; do
    user="$line"
    echo "Text read from file: $user"
    name="$(cut -d' ' -f 1 <<< $user)"
    pass="$(cut -d' ' -f 2 <<< $user)"
    echo "Name captured: $name"
    echo "password captured: $pass"
    passEncoded=$(echo -n "$pass" | openssl enc -base64)
    newUser="$name $passEncoded"
    echo "new user: $newUser"
    echo "$newUser" >> inputUser.txt
    
done < users.txt