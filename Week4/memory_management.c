/******************************************************************************

Crystal Sciarrino
CST-221 Memory Management
28 June, 2020

*******************************************************************************/



#include <stdio.h>



// Function that convert Decimal to binary 
int decToBinary(int n) 
{ 
    // Size of an integer is assumed to be 32 bits 
    for (int i = 31; i >= 0; i--) { 
        int k = n >> i; 
        if (k & 1) 
            printf( "1"); 
        else
            printf( "0"); 
    } 
} 

void decToHexa(int n) 
{    
    // char array to store hexadecimal number 
    char hexaDeciNum[100]; 
      
    // counter for hexadecimal number array 
    int i = 0; 
    while(n!=0) 
    {    
        // temporary variable to store remainder 
        int temp  = 0; 
          
        // storing remainder in temp variable. 
        temp = n % 16; 
          
        // check if temp < 10 
        if(temp < 10) 
        { 
            hexaDeciNum[i] = temp + 48; 
            i++; 
        } 
        else
        { 
            hexaDeciNum[i] = temp + 55; 
            i++; 
        } 
          
        n = n/16; 
    } 
     printf ("Ox"); 
    // printing hexadecimal number array in reverse order 
    for(int j=i-1; j>=0; j--) 
        
        printf("%c", hexaDeciNum[j]); 
} 


int main( ) {

   int c;

    //input and input prompt
   printf( "Enter a value between 0 and 4095:");
   scanf("%d", &c);
   
   //if the input does not fall within the criteria, the user must restart the
   //program
    if(c < 0 || c > 4095){
        printf("You entered an invalid value. Restart the program and input the correct value.");
    }
    else{
   printf( "\nYou entered: ");
   printf ("%d\n", c);
   
   //call decToBinary method using input value
   printf("Input value Binary representation\n");
   decToBinary(c);
    }
    //call decToHexa method using input value
    printf("\nInput value Hexadecimal representation\n");
    decToHexa(c);
    
   
  
   return 0;
}
