/*
Programmer: Crystal Sciarrino
This solution handles two calls to the world variable 'sum'
that ultimately is meant to result in a final value output to a thread
as '0'. Without the semaphore, the value would consistenly fluctuate
at runtime due to the fact both process are accessing and manipulating
'sum' at the same instances. This solution requires that the first
process to begin interacting with 'sum' must finish its computation
before the next process in the queue can access it.
*/
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>

//number of times the thread method loops
#define NUM_LOOPS 100000000

//world variable
long long sum =  0;

//initializing the Semaphore variable
sem_t mutex;

//Procedure 1 - x (x being the amount of calls to this procedure)
void* counting_thread(void *arg)
{
	//initializing the offset
	int offset = *(int *) arg;
	
	for (int i = 0; i < NUM_LOOPS; i++){
		/*start of critical section*/
		sem_wait(&mutex);
		sum += offset;
		/*end critical section*/
		sem_post(&mutex);
	}
	//exit with no return value
	pthread_exit(NULL);
}

//initializing code (main method)
int main (void)
{
	sem_init(&mutex, 0, 1);

	//spawn threads 1 and 2
	pthread_t id1;
	int offset1 = 1;
	pthread_create(&id1, NULL, counting_thread, &offset1);

	pthread_t id2;
	int offset2 = -1;
	pthread_create(&id2, NULL, counting_thread, &offset2);

	pthread_join(id1, NULL);
	pthread_join(id2, NULL);

	printf("Sum = %lld\n", sum);
	return 0;
}

