
/*
* Author:	Crystal Sciarrino
* Date:		06/21/2020
* Producer Consumer
*/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h> // A new header file added specfically for fork_2.c

int theProduct;
int buffer =1;
int max_calls = 8;
int num_calls = 0;


void producer(void);
void consumer(void);
int produce(void);
void consume(int i);
int get (void);
void put(int i);

void producer(void) {
	int i;
    //sleeps if consumer is running
	if(buffer == 1){
	  sleep (1);   
	}
    //set maximum number of calls to avoid infinite looping. if the number
    //of calls is less than the max it will produce
	if(num_calls < max_calls){
		i = produce();
		put(i);
	}

}

void consumer(void) {
	int i;
  //sleeps if producer is running
	if(buffer == 0){
	    sleep(1);
	}
    
    //set maximum number of calls to avoid infinite looping. if the number
    //of calls is less than the max it will consume
	if(num_calls < max_calls){
		i = get();
		consume(i);
	}
	
}



int produce(void){
	return theProduct++;
}

void consume(int i){
	printf("current product value  = %i\n", i);
	num_calls = num_calls + 1;
	producer();
	consumer();
}

int get (void){
    buffer = 0; 
    return theProduct;
    
}

void put(int i){
    
    theProduct = i;
    theProduct = i +1;
    buffer = 1;
    
}



int main(void)
{

    pid_t pid;

    printf("Process ID in main() is: %i\n\n", getpid());


    pid = fork(0);//specify process to be called. child or parent. This is currently calling the child process
    if (pid == -1) // Failed, no thread created
    {
        fprintf(stderr, "can't fork, error %d\n", errno);
        exit(EXIT_FAILURE);
    }
    else
    {
      
        if (pid == 0) // Child Process
        {
            printf("Currently Processing Child Process\n");
            producer();
            consumer();
        }
        else // Parent Process
        {
            printf("Currently Processing Parent Process\n");
            producer();
            consumer();
            
        }
    }

    return 0;
}